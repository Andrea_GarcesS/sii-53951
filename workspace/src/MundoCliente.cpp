//Autor: Andrea Garcés Sánchez

// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	pdatos->fin = true;

	int desproyeccion;
	desproyeccion = munmap(pdatos, sizeof(datos)); //desproyección de memoria
	if(desproyeccion < 0)
		perror("No se puede realizar la desproyección de memoria");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	int j;
	for(j=0;j<esferas.size();j++)
		esferas[j].Dibuja();

	int k;
	for(k=0;k<disparos.size();k++)
		disparos[k].Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{
	//RECEPCIÓN DE DATOS DEL SERVIDOR
	char cad[200];
	comunicacion.Receive(cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esferas[0].centro.x,&esferas[0].centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

	//MundoCliente se encarga de actualizar lo que no lee del servidor: el resto de esferas, los disparos, los avisos y los tiempos
	if(puntos1 == 3 || puntos2 == 3)
		exit(0);

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	int i;
	for(i=0;i<esferas.size();i++)
	{
		esferas[i].Mueve(0.025f);
		jugador1.Rebota(esferas[i]);
      		jugador2.Rebota(esferas[i]);
	        if(fondo_izq.Rebota(esferas[i]))
	        {
	                esferas[i].centro.x=0;
	                esferas[i].centro.y=rand()/(float)RAND_MAX;
	                esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
	                esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
		}

       		if(fondo_dcho.Rebota(esferas[i]))
	        {
	                esferas[i].centro.x=0;
	                esferas[i].centro.y=rand()/(float)RAND_MAX;
	                esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
	                esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
	        }
	}

	int j,k;
	for(j=0;j<paredes.size();j++)
	{
		for(k=0;k<esferas.size();k++)
			paredes[j].Rebota(esferas[k]);
		paredes[j].Rebota(jugador1);
		paredes[j].Rebota(jugador2);
	}

	int l;
	for(l=0;l<disparos.size();l++)
	{
		disparos[l].Mueve(0.025f);

		if(fondo_izq.Colision(disparos[l]) || fondo_dcho.Colision(disparos[l]))
		{
			disparos.erase(disparos.begin() + l); //se elimina el disparo que colisiona con alguno de los planos
			break;
		}

		if(jugador1.Colision(disparos[l]))
		{
			if((puntos1 + puntos2) % 2 == 0) //si la suma de puntos es par el jugador disminuye su tamaño
			{
				float longitud = (jugador1.y2 - jugador1.y1) * 0.95;
				float centro = (jugador1.y2 + jugador1.y1) * 0.5;
                                jugador1.y1 = centro + longitud * 0.5;
                                jugador1.y2 = centro - longitud * 0.5;
			}
			else //si es impar el jugador se queda parado
			{
				aviso_parada1 = true;
                                tiempo1 = tiempo;
			}

			disparos.erase(disparos.begin() + l);
			break;
		}

		if(jugador2.Colision(disparos[l]))
                {
                        if((puntos1 + puntos2) % 2 == 0)
                        {
                	        float longitud = (jugador2.y2 - jugador2.y1) * 0.95;
                                float centro = (jugador2.y2 + jugador2.y1) * 0.5;
                                jugador2.y1 = centro + longitud * 0.5;
                                jugador2.y2 = centro - longitud * 0.5;
                        }
                        else
                        {
				aviso_parada2 = true;
                                tiempo2 = tiempo;
                        }

                        disparos.erase(disparos.begin() + l);
			break;
                }
	}

	//EVENTOS TEMPORALES
	if(tiempo - tiempo1 < 80) //el jugador se queda parado durante 80 ciclos (2 segundos)
		jugador1.velocidad.y = 0;
	else
		aviso_parada1 = false;

	if(tiempo - tiempo2 < 80)
		jugador2.velocidad.y = 0;
	else
		aviso_parada2 = false;

	if(tiempo == tiempo_esfera && !aviso_esfera) //momento aleatorio entre 0 y 1000 ciclos (0 - 25 segundos)
	{
		aviso_esfera = true;
		Esfera e;
		esferas.push_back(e); //se añade una esfera
	}

	if(aviso_esfera) //el radio se reduce siempre que sea superior a 0.1
	{
		int i;
		for(i=0;i<esferas.size();i++)
			if(esferas[i].radio > 0.1f) esferas[i].radio -= 0.001f;
	}

	tiempo++; //contador de ciclos (1 ciclo cada 25 ms)

	//ACTUALIZACIÓN DE DATOS EN MEMORIA
	pdatos->esfera = esferas[0]; //el bot solo se centra en la primera esfera
        pdatos->raqueta1 = jugador1;

        if (pdatos->accion == 1) //la raqueta sube
                OnKeyboardDown ('w', 0, 0);
        if (pdatos->accion == -1) //la raqueta baja
                OnKeyboardDown ('s', 0, 0);
	if (pdatos->accion == 0) {} //no hace nada
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[100];
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(cad,"s");;break;
        case 'w':sprintf(cad,"w");break;
        case 'l':sprintf(cad,"l");break;
        case 'o':sprintf(cad,"o");break;

	//se dispara con las teclas a y p
/*	case 'a':
	{
		Disparo d;
		d.centro.x = jugador1.x2 + d.radio;
		d.centro.y = jugador1.y1 + (jugador1.y2 - jugador1.y1) / 2;
		disparos.push_back(d);
		break;
	}
	case 'p':
	{
                Disparo d;
                d.centro.x = jugador2.x1 - d.radio;
                d.centro.y = jugador2.y1 + (jugador2.y2 - jugador2.y1) / 2;
		d.velocidad.x = -4;
                disparos.push_back(d);
                break;
        }*/
	}
	comunicacion.Send(cad, sizeof(cad));
}

void CMundoCliente::Init()
{
	tiempo = 0;
	tiempo1 = -80; //marcador de tiempo del jugador para contar el tiempo que permanece parado
	tiempo2 = -80;
	tiempo_esfera = rand() % 1001; //número aleatorio entre 0 y 1000 

	aviso_parada1 = false; //avisos de eventos temporales inicializados en false
	aviso_parada2 = false;
	aviso_esfera = false;

	Esfera e;
	esferas.push_back(e); //el juego inicia con una esfera

	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//PROYECCIÓN EN MEMORIA
	int fd_mmap = open ("/tmp/datos", O_CREAT|O_TRUNC|O_RDWR, 0666);
        if (fd_mmap < 0)
        {
                perror("Error. No se puede abrir el fichero de datos");
        }
	int escritura;
        escritura = write (fd_mmap, &datos, sizeof(datos));
	if(escritura < 0)
		perror("No se puede realizar la escritura de datos en la proyección de memoria");
        pdatos = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(datos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0)); //conversión de void* a DatosCompartidos*
	if(pdatos == MAP_FAILED)
	{
		perror("Error. No se puede realizar la proyección en memoria");
	}
        close(fd_mmap);
	pdatos->fin = false;

	//CONEXIÓN CON EL SERVIDOR
        char ip[]="127.0.0.1";
        char nombre[100];
        printf("Nombre: \n");
        scanf("%s", nombre);
        comunicacion.Connect(ip, 8000);
        comunicacion.Send(nombre, sizeof(nombre));
}
