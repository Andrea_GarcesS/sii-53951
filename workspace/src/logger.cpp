#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main() {

        int fd_lectura_sl, tuberia_sl;
        typedef struct { //misma estructura que en Mundo
                int puntos1;
                int puntos2;
                int marcador;
        } Puntuacion;
        Puntuacion puntuacion;

	tuberia_sl = mkfifo("/tmp/tuberia_sl", 0600); //se crea la tubería (permisos RW solo para el dueño)

        if (tuberia_sl < 0)
        {
                perror("Error. No se puede crear la tubería servidor-logger");
        }
        else
                printf("Tubería servidor-logger creada correctamente\n");

	fd_lectura_sl = open ("/tmp/tuberia_sl", O_RDONLY); //se abre la tubería para lectura

        if (fd_lectura_sl < 0)
        {
                perror("Error. No se puede abrir para lectura la tubería servidor-logger");
        }

	while (read (fd_lectura_sl, &puntuacion, sizeof(puntuacion)) == sizeof(puntuacion)) //lee mientras el tamaño de lo leído sea el tamaño de la estructura
	{
	        if (puntuacion.marcador == 1)
      	        	printf ("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntuacion.puntos1);
  		else if (puntuacion.marcador == 2)
            		printf ("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntuacion.puntos2);
		if((puntuacion.puntos1 == 3) || (puntuacion.puntos2 == 3))
			printf ("Fin de la partida.\n");
	}

        close(fd_lectura_sl); //se cierra el extremo de lectura de la tubería
        unlink("/tmp/tuberia_sl"); //se elimina la tubería
        return 0;
}
