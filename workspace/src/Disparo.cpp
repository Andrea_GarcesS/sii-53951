//Autor: Andrea Garcés Sánchez

// Disparo.cpp: implementation of the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#include "Disparo.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Disparo::Disparo()
{
        radio = 0.2f;
        velocidad.x = 4;
        velocidad.y = 0;
}

Disparo::~Disparo()
{

}

void Disparo::Dibuja()
{
        glColor3ub(0, 255, 0);
        glEnable(GL_LIGHTING);
        glPushMatrix();
        glTranslatef(centro.x, centro.y, 0);
        glutSolidSphere(radio, 15, 15);
        glPopMatrix();
}

void Disparo::Mueve(float t)
{
        centro.x = centro.x + velocidad.x * t;
}
