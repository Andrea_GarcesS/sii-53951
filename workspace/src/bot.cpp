#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>

int main() {

        int fd_mmap;
        DatosMemCompartida *pdatos;

	fd_mmap = open("/tmp/datos", O_RDWR);

        if (fd_mmap < 0) {
                perror("Error. No se puede abrir el fichero de datos");
        }

        pdatos = static_cast<DatosMemCompartida*>(mmap (NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0));
	if(pdatos == MAP_FAILED)
		perror("Error. No se puede realizar la proyección en memoria");
        close(fd_mmap);
	pdatos->fin = false;

        while(1)
        {
                if (pdatos->esfera.centro.y < pdatos->raqueta1.y1) //baja cuando la esfera está por debajo de su límite inferior
                        pdatos->accion = -1;
                else if (pdatos->esfera.centro.y > pdatos->raqueta1.y2) //sube cuando la esfera está por encima de su límite superior
                        pdatos->accion = 1;
		else
			pdatos->accion = 0;
		if(pdatos->fin)
			break;
                usleep(25000); //en suspensión durante 25 ms
        }

	int desproyeccion;
	desproyeccion = munmap (pdatos, sizeof(DatosMemCompartida)); //desproyección de memoria
	if(desproyeccion < 0)
                perror("Error. No se puede realizar la desproyección de memoria");

        return 0;
}
