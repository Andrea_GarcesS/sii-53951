//Autor: Andrea Garcés Sánchez

// Disparo.h: interface for the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DISPARO_H
#define DISPARO_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Disparo
{
public:
        Disparo();
        virtual ~Disparo();

        Vector2D centro;
        Vector2D velocidad;
        float radio;

        void Dibuja();
        void Mueve(float t);
};

#endif
