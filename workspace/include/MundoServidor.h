//Autor: Andrea Garcés Sánchez

// MundoServidor.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Disparo.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	std::vector<Esfera> esferas; //al haber varias esferas, se implementa como vector
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	std::vector<Disparo> disparos; //se tiene un vector de disparos

	int puntos1;
	int puntos2;

	int tiempo;
	int tiempo1;
	int tiempo2;
	int tiempo_esfera;

	bool aviso_parada1;
	bool aviso_parada2;
	bool aviso_esfera;

	int fd_escritura_sl; //extremo de escritura de la tubería servidor-logger

	pthread_t thid1;

	void RecibeComandosJugador(); //función para el thread

	Socket conexion;
	Socket comunicacion;
};

#endif // !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
