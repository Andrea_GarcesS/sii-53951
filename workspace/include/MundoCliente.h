//Autor: Andrea Garcés Sánchez

// MundoCliente.h: interface for the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Disparo.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoCliente
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	std::vector<Esfera> esferas; //al haber varias esferas, se implementa como vector
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	std::vector<Disparo> disparos; //se tiene un vector de disparos

	int puntos1;
	int puntos2;

	int tiempo;
	int tiempo1;
	int tiempo2;
	int tiempo_esfera;

	bool aviso_parada1;
	bool aviso_parada2;
	bool aviso_esfera;

	DatosMemCompartida datos;
	DatosMemCompartida* pdatos;

	Socket comunicacion;
};

#endif // !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
